provider "aws" {
  region = "us-east-1"
}

# putting state file in s3
terraform {
  backend "s3" {
    bucket = "tf-state-challenge-unique"
    key    = "terraform.tfstate"
    region = "us-east-1"
  }
}

module "vpc" {
  source = "./modules/vpc"
}

module "s3" {
  source = "./modules/s3"
}

module "ecs" {
  source = "./modules/ecs"
}

# module "eks" {
#   source = "./modules/eks"
# }

module "elasticsearch" {
  source = "./modules/elasticsearch/"
}