provider "aws" {
  region = "us-east-1"
}
# Bucket name
resource "aws_s3_bucket" "tf-state-challenge-unique" {
  bucket = "tf-state-challenge-unique"
}
# Enables bucket versioning
resource "aws_s3_bucket_versioning" "versioning_example" {
  bucket = aws_s3_bucket.tf-state-challenge-unique.id
  versioning_configuration {
    status = "Enabled"
  }
}