resource "aws_elasticsearch_domain" "es-for-challenge" {
  domain_name           = "es-for-challenge"
  elasticsearch_version = "7.10"

  cluster_config {
    instance_type = "t3.small.elasticsearch"
  }
# cluster ebs config
  ebs_options {
    ebs_enabled = true
    volume_size = 20
  }
  tags = {
    Domain = "es-for-challenge"
  }
}

output "es-endpoint" {
  value = aws_elasticsearch_domain.es-for-challenge.endpoint
  description = "Elasticsearch domain endpoint"
}