# # Will come back to this later, need a working config

# variable "vpc_id" {}
# variable "subnet_ids" {}

# resource "aws_security_group" "web_server" {
#   name        = "example-web-server"
#   description = "Allow inbound HTTP traffic"
#   vpc_id      = var.vpc_id

#   ingress {
#     from_port   = 80
#     to_port     = 80

#     protocol   = "tcp"
#     cidr_blocks = ["0.0.0.0/0"]
#   }
# }

# resource "aws_instance" "web_server" {
#   ami           = "ami-0c5c5e8f7f9ac58b0" # Amazon Linux 2 LTS with Docker preinstalled
#   instance_type = "t2.micro"

#   key_name = "your_key_pair_name"

#   vpc_security_group_ids = [aws_security_group.web_server.id]
#   subnet_id              = var.subnet_ids[0]

#   user_data = <<-EOF
#               #!/bin/bash
#               sudo yum update -y
#               sudo yum install -y docker
#               sudo service docker start
#               sudo docker pull nginx
#               sudo docker run -d -p 80:80 --restart=always nginx
#               EOF

#   tags = {
#     Name = "web-server"
#   }
# }

# resource "aws_lb" "example" {
#   name               = "example-lb"
#   internal           = false
#   load_balancer_type = "application"
#   security_groups    = [aws_security_group.web_server.id]
#   subnets            = var.subnet_ids
# }

# resource "aws_lb_target_group" "example" {
#   name     = "example-lb-target-group"
#   port     = 80
#   protocol = "HTTP"
#   vpc_id   = var.vpc_id
# }

# resource "aws_lb_listener" "example" {
#   load_balancer_arn = aws_lb.example.arn
#   port              = 80
#   protocol          = "HTTP"

#   default_action {
#     type             = "forward"
#     target_group_arn = aws_lb_target_group.example.arn
#   }
# }

# resource "aws_lb_target_group_attachment" "example" {
#   target_group_arn = aws_lb_target_group.example.arn
#   target_id        = aws_instance.web_server.id
#   port             = 80
# }

# output "load_balancer_dns_name" {
#   value = aws_lb.example.dns_name
# }
