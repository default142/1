resource "aws_lb" "lb-for-challenge" {
  name               = "lb-for-challenge"
  internal           = false
  load_balancer_type = "application"
  subnets            = ["<your-subnet-id>"]
  security_groups    = ["<your-security-group-id>"]
}

resource "aws_lb_listener" "web_server_listener" {
  load_balancer_arn = aws_lb.lb-for-challenge.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_lb_target_group.web_server_target_group.arn
    type             = "forward"
  }
}

resource "aws_lb_target_group" "web_server_target_group" {
  name        = "web_server_target_group"
  port        = 80
  protocol    = "HTTP"
  target_type = "ip"

  health_check {
    path     = "/"
    protocol = "HTTP"
  }

  depends_on = [
    aws_lb.lb-for-challenge
  ]
}

resource "aws_lb_target_group_attachment" "web_server_attachment" {
  target_group_arn = aws_lb_target_group.web_server_target_group.arn
  target_id        = aws_ecs_service.web_server_service.id
  port             = 80
}
