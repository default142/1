# # Will come back to this later, need a working config

# # Network overview
# # Public Subnets:
# # Subnet 1: CIDR block "10.0.1.0/24" (IP range: 10.0.1.1 to 10.0.1.254)
# # Subnet 2: CIDR block "10.0.2.0/24" (IP range: 10.0.2.1 to 10.0.2.254)

# # Private Subnets:
# # Subnet 1: CIDR block "10.0.101.0/24" (IP range: 10.0.101.1 to 10.0.101.254)
# # Subnet 2: CIDR block "10.0.102.0/24" (IP range: 10.0.102.1 to 10.0.102.254)

# # Creates a vpc
# resource "aws_vpc" "my-vpc" {
#   cidr_block = "10.0.0.0/16"

#   tags = {
#     Name = "vpc-for-challenge"
#   }
# }

# # Creates two public subnets within the VPC created above. The count parameter is set to 2, which instructs Terraform 
# # to create multiple instances of this resource with varying configurations based on the count.index value.
# resource "aws_subnet" "public" {
#   count = 2

#   cidr_block = "10.0.${count.index + 1}.0/24"
#   vpc_id     = aws_vpc.main.id

#   tags = {
#     Name = "public-subnet-${count.index + 1}"
#   }
# }

# # Creates two private subnets within the VPC
# resource "aws_subnet" "private" {
#   count = 2 

#   cidr_block = "10.0.${count.index + 101}.0/24" #
#   vpc_id     = aws_vpc.main.id

#   tags = {
#     Name = "private-subnet-${count.index + 1}"
#   }
# }

# # Create a security group for the Elastic Load Balancer
# resource "aws_security_group" "elb" {
#   name        = "elb"
#   description = "Allow inbound traffic to the load balancer"
#   vpc_id      = aws_vpc.main.id
# }

# # The below outputs resource ids
# output "vpc_id" {
#   value = aws_vpc.main.id
# }

# output "public_subnets" {
#   value = aws_subnet.public.*.id
# }

# output "private_subnets" {
#   value = aws_subnet.private.*.id
# }

# output "elb_security_group_id" {
#   value = aws_security_group.elb.id
# }
