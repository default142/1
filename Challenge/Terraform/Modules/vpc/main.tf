# Creates a VPC

provider "aws" {
  region = "us-east-1"
}

resource "aws_vpc" "vpc-for-challenge" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = "vpc-for-challenge"
  }
}

# Creates 3 subnets
resource "aws_subnet" "subnet1" {
  vpc_id            = aws_vpc.vpc-for-challenge.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "us-east-1a"

  tags = {
    Name = "subnet1-for-challenge"
  }
}

resource "aws_subnet" "subnet2" {
  vpc_id            = aws_vpc.vpc-for-challenge.id
  cidr_block        = "10.0.2.0/24"
  availability_zone = "us-east-1b"

  tags = {
    Name = "subnet2-for-challenge"
  }
}

resource "aws_subnet" "subnet3" {
  vpc_id            = aws_vpc.vpc-for-challenge.id
  cidr_block        = "10.0.3.0/24"
  availability_zone = "us-east-1c"

  tags = {
    Name = "subnet3-for-challenge"
  }
}

#Creates the internet gateway
resource "aws_internet_gateway" "gw-for-challenge" {
  vpc_id = aws_vpc.vpc-for-challenge.id
  tags = {
    Name = "internet-gateway-for-challenge"
  }
}

# Creates a route table, routes to igw
resource "aws_route_table" "rt-for-challenge" {
  vpc_id = aws_vpc.vpc-for-challenge.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw-for-challenge.id
  }

  tags = {
    Name = "public-route-table-for-challenge"
  }
}

# Associates route tables to subnets
resource "aws_route_table_association" "subnet1_association" {
  subnet_id      = aws_subnet.subnet1.id
  route_table_id = aws_route_table.rt-for-challenge.id
}

resource "aws_route_table_association" "subnet2_association" {
  subnet_id      = aws_subnet.subnet2.id
  route_table_id = aws_route_table.rt-for-challenge.id
}

resource "aws_route_table_association" "subnet3_association" {
  subnet_id      = aws_subnet.subnet3.id
  route_table_id = aws_route_table.rt-for-challenge.id
}

# Outputs
output "vpc-for-challenge-id" {
  value = aws_vpc.vpc-for-challenge.id
}

output "subnets-for-challenge" {
  value = [aws_subnet.subnet1.id, aws_subnet.subnet2.id, aws_subnet.subnet3.id]
}