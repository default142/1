module "vpc" {
    source = "../vpc"
}

module "elasticsearch" {
    source = "../elasticsearch"
}

# Creates a security group for the ALB, allowing inbound traffic on port 80 (HTTP) from any IP address (0.0.0.0/0)
resource "aws_security_group" "lb_sg" {
  name        = "nginx-lb-sg"
  description = "Allow inbound traffic to Nginx ALB"
  vpc_id      = module.vpc.vpc-for-challenge-id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Creates a security group for the Fargate service, allowing inbound traffic on port 80 (HTTP) from the ALB security group.
resource "aws_security_group" "fargate_sg" {
  name        = "nginx-fargate-sg"
  description = "Allow inbound traffic from ALB to Fargate"
  vpc_id      = module.vpc.vpc-for-challenge-id

  ingress {
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [aws_security_group.lb_sg.id]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_lb" "nginx_alb" {
  name               = "nginx-alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.lb_sg.id]
  subnets            = module.vpc.subnets-for-challenge

}

resource "aws_lb_target_group" "nginx_tg" {
  name     = "nginx-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = module.vpc.vpc-for-challenge-id
  target_type = "ip" 

  health_check {
    enabled             = true
    healthy_threshold   = 3
    unhealthy_threshold = 3
    interval            = 30
    path                = "/"
    port                = "traffic-port"
    protocol            = "HTTP"
    timeout             = 5
  }
}


resource "aws_lb_listener" "nginx_listener" {
  load_balancer_arn = aws_lb.nginx_alb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_lb_target_group.nginx_tg.arn
    type             = "forward"
  }
}

resource "aws_ecs_cluster" "nginx_cluster" {
  name = "nginx-cluster"
}

resource "aws_ecs_task_definition" "nginx_task" {
  family                   = "nginx-task"
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = "512"
  memory                   = "1024"
  execution_role_arn       = aws_iam_role.ecs_execution_role.arn
  task_role_arn            = aws_iam_role.ecs_task_role.arn

  container_definitions = jsonencode([
    {
      name  = "nginx"
      image = "nginx:latest"

      portMappings = [
        {
          containerPort = 80
          hostPort      = 80
          protocol      = "tcp"
        }
      ]
      # Enable FireLens for log routing
      logConfiguration = {
        logDriver = "awsfirelens"
        options = {
          Name = "es"
          Match = "*"
          Host = module.elasticsearch.es-endpoint
          Port = "443"
          Index = "logs"
          Type = "_doc"
          Aws_Auth = "On"
          Aws_Region = "us-east-1"
          tls = "on"
        }
      }
    },
    {
      # FireLens container
      name  = "log_router"
      image = "amazon/aws-for-fluent-bit:latest"
      user  = "0"
      firelensConfiguration = {
        type = "fluentbit"
      }
      cpu = 10
      memory = 50
      memoryReservation = 50
      essential = false
      logConfiguration = {
        logDriver = "awslogs"
        options = {
          "awslogs-group"         = aws_cloudwatch_log_group.firelens_logs.name
          "awslogs-region"        = "us-east-1"
          "awslogs-create-group"  = "true"
          "awslogs-stream-prefix" = "firelens"
        }
      }
    }
  ])
}

resource "aws_cloudwatch_log_group" "firelens_logs" {
  name = "/ecs/firelens/nginx_logs"
}

# Iams
resource "aws_iam_policy" "firelens_cloudwatch_policy" {
  name = "firelens_cloudwatch_policy"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "logs:CreateLogStream",
          "logs:CreateLogGroup",
          "logs:PutLogEvents"
        ]
        Effect   = "Allow"
        Resource = "arn:aws:logs:*:*:*"
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "firelens_cloudwatch_policy" {
  policy_arn = aws_iam_policy.firelens_cloudwatch_policy.arn
  role       = aws_iam_role.ecs_task_role.name
}

resource "aws_iam_role" "ecs_execution_role" {
  name = "ecs_execution_role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "ecs-tasks.amazonaws.com"
        }
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "ecs_execution_role_policy" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
  role       = aws_iam_role.ecs_execution_role.name
}

resource "aws_iam_role" "ecs_task_role" {
  name = "ecs_task_role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "ecs-tasks.amazonaws.com"
        }
      }
    ]
  })
}

resource "aws_ecs_service" "nginx_service" {
  name            = "nginx-service"
  cluster         = aws_ecs_cluster.nginx_cluster.id
  task_definition = aws_ecs_task_definition.nginx_task.arn
  desired_count   = 1
  launch_type     = "FARGATE"

  network_configuration {
    assign_public_ip = true
    subnets          = module.vpc.subnets-for-challenge
    security_groups  = [aws_security_group.fargate_sg.id]
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.nginx_tg.arn
    container_name   = "nginx"
    container_port   = 80
  }

  depends_on = [aws_lb_listener.nginx_listener]
}
