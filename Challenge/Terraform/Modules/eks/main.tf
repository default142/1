# Will try eks again after I get a working nginx deployment..
# module "vpc" {
#     source = "../vpc"
# }
# provider "kubernetes" {
#     host                   = module.eks.cluster_endpoint
#     cluster_ca_certificate = base64decode(module.eks.cluster_certificate_authority_data)
# }

# module "eks" {
#   source                 = "terraform-aws-modules/eks/aws"
#   version                = "19.13.0"
#   cluster_version        = "1.26"
#   cluster_name           = "eks-cluster-for-challenge"
#   cluster_endpoint_public_access = true
#   tags                   = {
#     Name = "eks-cluster-for-challenge"
#   }   
#   subnet_ids             = module.vpc.subnets-for-challenge
#   vpc_id                 = module.vpc.vpc-for-challenge-id
# #  write_kubeconfig       = true
# #  manage_aws_auth        = true
# #  map_roles              = []
# #  map_users              = []
# }


# # Creates a Sg for the ELB below
# resource "aws_security_group" "webserver_sg" {
#   name_prefix = "webserver-"
#   vpc_id = module.vpc.vpc-for-challenge-id

#   ingress {
#     from_port = 80
#     to_port   = 80
#     protocol  = "tcp"
#     cidr_blocks = ["0.0.0.0/0"]
#   }
# }

# # Creates an ELB
# resource "aws_elb" "elb-for-challenge" {
#   name               = "elb-for-challenge"
#   subnets            = module.vpc.subnets-for-challenge
#   security_groups    = [aws_security_group.webserver_sg.id]
#   listener {
#     instance_port     = 80
#     instance_protocol = "http"
#     lb_port           = 80
#     lb_protocol       = "http"
#   }
# }

# # Creates k8s namespace
# resource "kubernetes_namespace" "nginx" {
#   metadata {
#     name = "nginx"
#   }
# }

# # Creates a k8s deployment resource
# resource "kubernetes_deployment" "nginx" {
#   metadata {
#     name = "nginx"
#     namespace = kubernetes_namespace.nginx.metadata[0].name
#   }

#   spec {
#     replicas = 2

#     selector {
#       match_labels = {
#         app = "nginx"
#       }
#     }

#     template {
#       metadata {
#         labels = {
#           app = "nginx"
#         }
#       }

#       spec {
#         container {
#           image = "nginx:latest"
#           name  = "nginx"
#           port {
#             container_port = 80
#           }
#         }
#       }
#     }
#   }
  
#   depends_on = [kubernetes_namespace.nginx]
# }

# resource "kubernetes_service" "nginx" {
#   metadata {
#     name = "nginx"
#     namespace = kubernetes_namespace.nginx.metadata[0].name
#   }

#   spec {
#     selector = {
#       app = "nginx"
#     }

#     port {
#       name        = "http"
#       port        = 80
#       target_port = 80
#     }

#     type = "LoadBalancer"
#   }

#   depends_on = [kubernetes_deployment.nginx]
# }