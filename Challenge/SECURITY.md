Stopped root user access, enable mfa and switched to another user
Enforced strongest password policy, 12 chars, 60 day expiration, password reuse prevention
Enabled CloudTrail
Enabled GuardDuty #Security monitoring service that analyzes and processes Foundational data sources, such as AWS CloudTrail 
Enabled SecurityHub #Run automated security checks across your AWS environment, Prioritize and remediate security issues
Enabled Config #Summarized view of AWS and non-AWS resources and the compliance status of the rules and the resources in each AWS Region

Moved the state file to s3

Future improvements - 
Enable SSO for users through okta and identity center, this way we can remove iam passwords
lockdown ports on everywhere, some stuff is 0.0.0.0/0