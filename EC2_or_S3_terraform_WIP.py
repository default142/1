import sys
from time import sleep
import boto3
import webbrowser

#This is a tool that builds out infrastructure using boto3 based off user input, its more of a learning project so it contains "easter eggs"
#For this to work you will need aws cli configured. See - https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html
#You will also need a default VPC in the respective region. See - https://docs.aws.amazon.com/vpc/latest/userguide/default-vpc.html#create-default-vpc

#some functions

def err():
    print('You did something wrong, please restart')
    sys.exit()

def region():
    regionanswer = input('Type \n1 for us east-1\n2 for us east-2\n3 for us west-1\n\n>>')
    return regionanswer

# def instancetype():
#     instancetypeanswer = input('Type\n1 for t2.micro\n2 for t2.large\n3 for t9000.SUPERlarge\n\n>>')
#     return instancetypeanswer
# below seems like cleaner code
def instancetype():
    return input('Type\n1 for t2.micro\n2 for t2.large\n3 for t9000.SUPERlarge\n\n>>')

def fire():
    fire = 6
    for a in range(fire):
        for b in range(a + 1):
            print("🔥", end=' ')
        print()

def reversefire():
    fire = 6
    for a in range(fire):
        for b in range(fire - a, 0, -1):
            print('🔥', end=' ')
        print()


########################################################
s3answer = None
ec2answer = None
ec2regionanswer = None
s3regionanswer = None
s3websiteanswer = None

######                           The goal of this script is to make an ec2 instance or s3 bucket
#####                            If you go the ec2 route it allows 3 different regions, 1 instance type for testing and I also added a webbrowser for learning purposes
####                             If you go the s3 route it can also make a static website, creating a index.html locally and configuring the bucket appropriately        

print('Do you want an EC2 Instance or S3 Bucket?')
ec2ors3 = input('Please Type\n1 for EC2\n2 for S3\n\n>>')

if ec2ors3 == '1':
    ec2answer = input('Type yes to confirm EC2\n\n>>').lower()
    if ec2answer != ('yes').lower():
        err()
elif ec2ors3 == '2':
    s3answer = input('Type yes to confirm S3\n\n>>').lower()
    if s3answer != ('yes').lower():
        err()
else:
    err()

#####                                 ec2 starts here
if ec2answer == 'yes':
    print('\nOk we are making an ec2')
    print('Please confirm the region')
    ec2regionanswer = region()
    if ec2regionanswer == '1':
        print('\nMaking ec2 in us east-1')
    elif ec2regionanswer == '2':
        print('\nMaking ec2 in us east-2')
    elif ec2regionanswer == '3':
        print('\nMaking ec2 in us west-1')
    else:
        err()

if ec2answer == 'yes':
    print('\nPlease choose instance type\nFor testing purposes anything other than 1 will error out')
    ec2instancetype = instancetype()
    #if you are just looking at the code and not going to run this, 84-90 is just a meme
    if ec2instancetype == '3':
        print('\n'* 10)
        fire()
        print('What did you do!?! \n     AHHHHHH')
        reversefire()
        sleep(2)
        webbrowser.open('https://www.youtube.com/watch?v=SiMHTK15Pik')
    elif ec2instancetype == '1':
        print('\nMaking a t2.micro')
    elif ec2instancetype != '1':
        err()
    if ec2regionanswer == '1' and ec2instancetype == '1':
        ec2 = boto3.resource('ec2',region_name = "us-east-1")
        instance = ec2.create_instances(
            MinCount = 1,
            MaxCount = 1,
            ImageId="ami-0cff7528ff583bf9a",
            InstanceType = 't2.micro',
        )
    if ec2regionanswer == '2' and ec2instancetype == "1":
        ec2 = boto3.resource('ec2',region_name = "us-east-2")
        instance = ec2.create_instances(
            MinCount = 1,
            MaxCount = 1,
            ImageId="ami-02d1e544b84bf7502",
            InstanceType = 't2.micro',
        )
    if ec2regionanswer == '3' and ec2instancetype == "1":
        ec2 = boto3.resource('ec2',region_name = "us-west-1")
        instance = ec2.create_instances(
            MinCount = 1,
            MaxCount = 1,
            ImageId="ami-0d9858aa3c6322f73",
            InstanceType = 't2.micro',
        )


# This below code(95-108) is supposed to be a shortened version of above(63 - 86)
# Im trying to shorten it with an if condition and the respective ec2regionanswer, not sure if its possible need to test
# if ec2answer == 'yes':
#     ec2 = boto3.resource('ec2')
#     instance = ec2.create_instances(
#         MinCount = 1,
#         MaxCount = 1,
#         if ec2regionanswer == 1:,
#              ImageId="ami-0cff7528ff583bf9a",
#              region_name = "us-east-1"
#         elif ec2regionanswer == 2:
#              ImageId="ami-02d1e544b84bf7502",
#              region_name = "us-east-2"
#         elif ec2regionanswer == 3:
#              ImageId="ami-0d9858aa3c6322f73",
#              region_name = "us-west-1"
#         InstanceType = 't2.micro'
##         InstanceType = instancetypeanswer,
#     )




#####                                  S3 starts here
if s3answer == 'yes':
    print('\nOk we are making an s3')
    s3regionanswer = region()
    if s3regionanswer == '1':
        print('Making s3 in us east-1')
    elif s3regionanswer == '2':
        print('Making s3 in us east-2')
    elif s3regionanswer == '3':
        print('Making s3 in us west-1')
    else:
        err()

if s3answer == 'yes':
    s3bucketname = input('What would you like the s3 bucket to be named?(Spaces are not allowed)\n\n>>')
    if s3regionanswer == '1':
        s3 = boto3.resource('s3')
        s3.create_bucket(Bucket = s3bucketname)
##### Spent a BIT of time troubleshooting why if I specified the location contraint I would get an error (thought it was some syntax err)
##### Turns out if you specify the default region it shows error "InvalidLocationConstraint"  (very helpful error btw XD)
##### see https://github.com/boto/boto3/issues/125 for more information
    if s3regionanswer == '2':
        s3 = boto3.resource('s3',region_name = "us-east-2")
        s3.create_bucket(Bucket = s3bucketname, CreateBucketConfiguration = {'LocationConstraint': "us-east-2"})
    if s3regionanswer == '3':
        #### Im not sure if/why we need to specify the region twice?
        s3 = boto3.resource('s3',region_name = "us-west-1")
        s3.create_bucket(Bucket = s3bucketname, CreateBucketConfiguration = {'LocationConstraint': "us-west-1"})

####                  bucket option is here 
if s3answer == 'yes':
    print('\n\nYour bucket has been created')
    s3websiteanswer = input('\nDo you want to make a static website with that bucket?\nType\nyes or no\n\n>>').lower()

if s3websiteanswer == 'yes':
    s3 = boto3.client('s3')
    s3.put_bucket_website(
        Bucket = s3bucketname,
        WebsiteConfiguration = {
        'ErrorDocument': {'Key': 'error.html'},
        'IndexDocument': {'Suffix': 'index.html'},
        }
)
#if they choose to create a website the below creates an index.html and uploads it to the bucket, could do the same for error.html
if s3websiteanswer == 'yes':
    with open("index.html", "w") as f:
        f.write('<html xmlns="http://www.w3.org/1999/xhtml" >\n<head>\n    <title>My Website Home Page</title>\n</head>\n<body>\n  <h1>Welcome to my website</h1>\n  <p>Now hosted on Amazon S3!</p>\n</body>\n</html>')
        
if s3websiteanswer == 'yes':
    s3 = boto3.client('s3')
    with open("index.html", "rb") as f:
        s3.upload_fileobj(f, s3bucketname, "index.html")
        sleep(1)
        #the line below this gives the index.html public access
        #this works but for some reason bucket url doesnt load index.html properly
        #the html file creates properly, error.html works too
        boto3.resource('s3').ObjectAcl(s3bucketname,'index.html').put(ACL='public-read')

#Im sure I can use an api request to get the url, instead of having them find it
if s3websiteanswer == 'yes':
    print('website created please login to console to get url')

if s3websiteanswer == 'no':
    print('\n\nOk, well this is the end.. Goodbye')

if ec2answer == None and s3answer == None:
    err()

# some notes
# ways to make this better or expand on the above - 

# use pyinput(this allows many features, such as choices)
# create containers using similar code as above, ex. nginx, apache, maybe a wordpress site?
# clean up the code, make it more readable... something like ec2answer == 1 should be ec2answer == yes or makeec2 == True
# create a gui with tkinter