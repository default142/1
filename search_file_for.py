#gives access in read mode
f = open('log.txt', 'r')

#opens files and reads it
d = f.read()

#this lowers all the characters in file, incase searched text is capitalized
#comment below out if you dont want to account for lowercase
dlow = d.lower()

#counts searched text
joe = dlow.count('joe')
joseph = d.count('joseph')


print('number of times joe is in log.txt:', joe)
print('number of times joseph is in log.txt:', joseph)

# contents of log file
# joe
# joseph
# joe
# Joe
# Joe
# Joseph

#output of python
# number of times joe is in log.txt: 4
# number of times Joseph is in log.txt: 1
