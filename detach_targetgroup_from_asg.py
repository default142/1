# This detaches a list of TG from their respective ASG's. You must know and input both

import boto3

autoscaling = boto3.client('autoscaling')

target_groups = {
    'TG1 HERE': 'ASG1 HERE',
    'TG2 HERE': 'ASG2 HERE'
    # Add more target groups and ASGs as needed, make sure to add ,
}

# Detach target groups from Auto Scaling groups
for target_group_arn, asg_name in target_groups.items():
    try:
        response = autoscaling.detach_load_balancer_target_groups(
            AutoScalingGroupName=asg_name,
            TargetGroupARNs=[target_group_arn]
        )
        print(f"Detached target group: {target_group_arn} from ASG: {asg_name}")
    except Exception as e:
        print(f"Failed to detach target group: {target_group_arn} from ASG: {asg_name}")
        print(f"Error: {e}")