# This is a template to get started automating things
# Make sure dependencies like pyautogui and selenium are able to import properly, you may have to install them with pip
# Selenium can be tricky to get working you will need to edit your path variables, see https://www.selenium.dev/documentation/webdriver/getting_started/install_drivers/
# There will be a lot of comments in this to explain what certain code does, hopefully not needing to pull up documentation
import sys
import pyperclip as clip
import random
import time
import pyautogui as auto
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
import webbrowser as web

def wait(x):
    time.sleep(x)

# gives you a buffer before starting
wait(3)

# waits a random period of time in seconds, to change that time see lines 21/22
# waittime = random.randint(5,20)
# this changed the random time from 1-7 seconds to 5-20 seconds 
def waitRandom():
    waittime = random.randint(1,7)
    wait(waittime)

# types a random message
def typeSomethingRandom():
    randomMessage = ('Hello!', 'I\'m automating stuff', 'You\'re message here')
    auto.typewrite(random.choice(randomMessage), interval=.10)
    auto.press('enter')

# could be used to login, for the credentials we should be able to pull from a file instead of hardcoding in plaintext, will come back to this
# on line 37 and 39 the interval lets you change time in seconds between keypresses
def login():
    username = ('admin')
    password = ('mypassword!')
    auto.typewrite(username, interval=.10)
    auto.press('tab')
    auto.typewrite(password, interval=.10)
    auto.press('enter')

# creates a text file, writes to it, copies the content and then pastes it
# what else could we put into that file? maybe we scrape information from the web and paste it somewhere
# we wouldn't really need it in a file to copy it, the file does help us log copied content though!(if we append)
def copyAndPasteFile():
    with open('copyAndPasteFile.txt', "w") as f:
        f.write('You can put text here, maybe we can write other stuff!')
    file = open('copyAndPasteFile.txt', 'r').read()
    clip.copy(file)
    pasteClip = clip.paste()
    auto.typewrite(pasteClip, interval =.10)

# opens google in a browser, searches for an animal picture from wikipedia saves it to the desktop for later use
# since this is in a function the browser if opened with browser.get closes after function is over, can we stop this?
# wip
def browserauto():
    # web.open('https://google.com')
    # wait(1)
    # auto.typewrite('animal', interval=.05)
    # wait(.5)
    # auto.press('enter')
    # wait(2.5)
    browser = webdriver.Chrome()
    browser.get('https://google.com')
    wait(1)
    browser.maximize_window()
    auto.typewrite('animal', interval=.1)
    wait(.5)
    auto.press('enter')
    wait(3)
    clickImages =  browser.find_element(By.CSS_SELECTOR, '#hdtb-msb > div:nth-child(1) > div > div:nth-child(2) > a')
    clickImages.click()
    wait(3)
    clickWiki = browser.find_element(By.PARTIAL_LINK_TEXT, 'Wikipedia')
    clickWiki.click()
    wait(3)

    # having trouble getting selenium to grab the animal_diversity.png, wikipedia site breaks that picture into ~9-12 different links
    # pretty sure thats messing below code up(82-84).. so instead we are just going to screen shot(85-90)
    # with open('animal.png', 'wb') as f:
    #    pic = browser.find_element(By.PARTIAL_LINK_TEXT,'Animal_diversity.png')
    #    f.write(pic.screenshot_as_png)
    tab1 = browser.switch_to.window(browser.window_handles[0])
    tab1
    # this assigns the tabs, letting us switch driver focus quickly
    tab2 = browser.switch_to.window(browser.window_handles[1])
    tab2 
    browser.save_screenshot('animal.png')
    wait(3)
    # WELLLL while doing line 85-90 found out the chromedriver is still "focused" on tab 1, even though we see tab 2 when clicking into wikipedia
    # so that might be why code 82-84 doesnt work right, we need to have the driver focus on tab 2 then find animal_diversity.png

    # got it to work!! had to switch find element to css selector(which I tried while debugging 82-84), just needed to have driver focus on wikipedia tab
    with open('animaltabfocus.png', 'wb') as f:
       pic = browser.find_element(By.CSS_SELECTOR,'#mw-content-text > div.mw-parser-output > table.infobox.biota > tbody > tr:nth-child(2) > td > div > img')
       f.write(pic.screenshot_as_png)
    # think we can short 96-98 to something like line 90, will come back to this
    
    
    


# typeSomethingRandom()
# waitRandom()
# login()
# copyAndPasteFile()
browserauto()