while True:
    ask_user = input("Type add, show, edit, complete or exit: ")
    ask_user = ask_user.strip()

    if "add" in ask_user or "new" in ask_user:
        todo = f"{ask_user[4:]}\n"
        
        with open("todos.txt", "r") as file:
            todos = file.readlines()
        
        todos.append(todo)

        with open("todos.txt", "w") as file:
                file.writelines(todos)
            
    elif "show" in ask_user:
        file = open("todos.txt", "r")
        todos = file.readlines()    
        file.close()

        for index, item in enumerate(todos):
            item = item.strip("\n")
            show = f"{index + 1}-{item.capitalize()}"
            print(show)

    elif "edit" in ask_user:
        number = int(ask_user[5:])

        with open("todos.txt", "r") as file:
            todos = file.readlines()
    
        todos[number - 1] =  input("Enter new todo: " + "\n")

        with open("todos.txt", "w") as file:
                file.writelines(todos)

    elif "complete" in ask_user:
        number = int(ask_user[9:])
        
        with open("todos.txt", "r") as file:
            todos = file.readlines()

        index = number - 1
        todo_removed = todos[index].strip("\n")
        todos.pop(index)

        with open("todos.txt", "w") as file:
                file.writelines(todos)

        print(f"This todo {todo_removed} was removed from the list")

    elif "exit" in ask_user:
        break

    else:
        print("Invalid command")
print("Goodbye")