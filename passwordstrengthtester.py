#challenge
#ask user for password and return strength of password

password = str(input("Enter you're password: "))
count = len(password)
#checks uppercase

uppercase = False
for char in password:
    if char.isupper():
        uppercase = True
        break
digit = False
for char in password:
    if char.isdigit():
        digit = True
        break

if count < 6:
    strength = "low, try making your password longer"

elif count > 11 and ("!" or "@" or "#" or "$" or "%" or "^" or "&" or "*") in password and uppercase and digit == True:
    strength = "extremely Strong"

elif count < 11 and ("!" or "@" or "#" or "$" or "%" or "^" or "&" or "*") in password and uppercase and digit == True:
    strength = "very strong, to make it better add more characters"

elif count > 6 and ("!" or "@" or "#" or "$" or "%" or "^" or "&" or "*") in password or uppercase or digit == True:
    strength = "strong, to improve try adding symbols or uppercase or numbers"

else:
    strength = "very low, try making your password longer adding uppercase or symbols"

print(f"You're password is {count} characters long and the strength is {strength}")
